package com.example.demo.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.CMenu;

public interface iMenuRespository extends JpaRepository <CMenu , Long>{
    
}
