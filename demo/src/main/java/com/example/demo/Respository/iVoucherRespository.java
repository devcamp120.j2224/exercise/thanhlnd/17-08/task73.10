package com.example.demo.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.CVoucher;

public interface iVoucherRespository extends JpaRepository <CVoucher , Long>{
    
}
