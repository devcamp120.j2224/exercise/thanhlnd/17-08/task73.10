package com.example.demo.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.CCountry;

public interface iCountryRespository extends JpaRepository <CCountry , Long> {
   boolean existsCountryByCountryCode(String countryCode);
}
